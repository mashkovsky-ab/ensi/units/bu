<?php

namespace App\Http\ApiV1\Modules\Sellers\Resources;

use App\Http\ApiV1\Modules\SellerUsers\Resources\OperatorsResource;
use App\Http\ApiV1\Modules\Stores\Resources\StoresResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class SellersResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'legal_name' => $this->legal_name,
            'external_id' => $this->external_id,
            'legal_address' => $this->legal_address,
            'fact_address'  => $this->fact_address,
            'inn' => $this->inn,
            'kpp' => $this->kpp,
            'payment_account' => $this->payment_account,
            'bank' => $this->bank,
            'bank_address' => $this->bank_address,
            'bank_bik' => $this->bank_bik,
            'correspondent_account' => $this->correspondent_account,
            'status' => $this->status,
            'status_at' => $this->status_at,
            'manager_id' => $this->manager_id,
            'storage_address' => $this->storage_address,
            'site' => $this->site,
            'can_integration' => $this->can_integration,
            'sale_info' => $this->sale_info,
            'city'  => $this->city,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'operators' => OperatorsResource::collection($this->whenLoaded('operators')),
            'stores' => StoresResource::collection($this->whenLoaded('stores')),
        ];
    }
}
