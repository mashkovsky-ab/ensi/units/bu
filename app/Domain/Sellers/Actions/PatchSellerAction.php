<?php

namespace App\Domain\Sellers\Actions;

use App\Domain\Sellers\Models\Seller;
use Illuminate\Support\Arr;

class PatchSellerAction
{
    public function execute(int $sellerId, array $fields): Seller
    {
        $seller = Seller::findOrFail($sellerId);
        $seller->update(Arr::only($fields, Seller::FILLABLE));

        return $seller;
    }
}
