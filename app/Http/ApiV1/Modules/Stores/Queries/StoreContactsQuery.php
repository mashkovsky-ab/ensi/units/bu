<?php

namespace App\Http\ApiV1\Modules\Stores\Queries;

use App\Domain\Stores\Models\StoreContact;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class StoreContactsQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = StoreContact::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedIncludes([]);

        $this->allowedSorts([
            'id',
            'store_id',
            'name',
            'phone',
            'email',
            'created_at',
            'updated_at',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('store_id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('phone'),
            AllowedFilter::exact('email'),
        ]);

        $this->defaultSort('id');
    }
}
