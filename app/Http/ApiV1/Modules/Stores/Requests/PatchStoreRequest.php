<?php

namespace App\Http\ApiV1\Modules\Stores\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchStoreRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'seller_id' => ['nullable', 'integer'],
            'xml_id' => ['nullable', 'string'],
            'active' => ['nullable', 'boolean'],
            'name' => ['nullable', 'string'],
            'address' => ['nullable', 'array'],
            'address.address_string' => ['nullable', 'string'],
            'address.country_code' => ['nullable', 'string'],
            'address.post_index' => ['nullable', 'string'],
            'address.region' => ['nullable', 'string'],
            'address.region_guid' => ['nullable', 'string'],
            'address.city' => ['nullable', 'string'],
            'address.city_guid' => ['nullable', 'string'],
            'address.street' => ['nullable', 'string'],
            'address.house' => ['nullable', 'string'],
            'address.block' => ['nullable', 'string'],
            'address.flat' => ['nullable', 'string'],
            'address.porch' => ['nullable', 'string'],
            'address.intercom' => ['nullable', 'string'],
            'address.floor' => ['nullable', 'string'],
            'address.comment' => ['nullable', 'string'],
            'address.geo_lat' => ['nullable', 'string'],
            'address.geo_lon' => ['nullable', 'string'],
            'timezone' => ['nullable', 'string'],
      ];
    }
}
