<?php

namespace App\Domain\Stores\Models;

use Carbon\Carbon;
use Database\Factories\Stores\StoreContactFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Класс-модель для сущности "Склады - контакты"
 * Class StoreContact
 *
 * @property int $id
 * @property int $store_id - id склада
 * @property string $name - имя
 * @property string $phone - телефон
 * @property string $email - email
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class StoreContact extends Model
{
    use HasFactory;

    /**
     * Заполняемые поля модели
     */
    const FILLABLE = [
        'store_id', 'name', 'phone', 'email',
    ];

    protected $fillable = self::FILLABLE;

    protected $table = 'store_contacts';

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return StoreContactFactory::new();
    }
}
