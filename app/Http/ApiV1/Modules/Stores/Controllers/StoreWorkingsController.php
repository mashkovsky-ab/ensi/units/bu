<?php

namespace App\Http\ApiV1\Modules\Stores\Controllers;

use App\Domain\Stores\Actions\CreateStoreWorkingAction;
use App\Domain\Stores\Actions\DeleteStoreWorkingAction;
use App\Domain\Stores\Actions\PatchStoreWorkingAction;
use App\Domain\Stores\Actions\ReplaceStoreWorkingAction;
use App\Http\ApiV1\Modules\Stores\Queries\StoreWorkingsQuery;
use App\Http\ApiV1\Modules\Stores\Requests\CreateOrReplaceStoreWorkingRequest;
use App\Http\ApiV1\Modules\Stores\Requests\PatchStoreWorkingRequest;
use App\Http\ApiV1\Modules\Stores\Resources\StoreWorkingsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class StoreWorkingsController
{
    public function create(CreateOrReplaceStoreWorkingRequest $request, CreateStoreWorkingAction $action)
    {
        return new StoreWorkingsResource($action->execute($request->validated()));
    }

    public function get(int $storeWorkingId, StoreWorkingsQuery $query)
    {
        return new StoreWorkingsResource($query->findOrFail($storeWorkingId));
    }

    public function replace(int $storeWorkingId, CreateOrReplaceStoreWorkingRequest $request, ReplaceStoreWorkingAction $action)
    {
        return new StoreWorkingsResource($action->execute($storeWorkingId, $request->validated()));
    }

    public function patch(int $storeWorkingId, PatchStoreWorkingRequest $request, PatchStoreWorkingAction $action)
    {
        return new StoreWorkingsResource($action->execute($storeWorkingId, $request->validated()));
    }

    public function delete(int $storeWorkingId, DeleteStoreWorkingAction $action)
    {
        $action->execute($storeWorkingId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, StoreWorkingsQuery $query)
    {
        return StoreWorkingsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
