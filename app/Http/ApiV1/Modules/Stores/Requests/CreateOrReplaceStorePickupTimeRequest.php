<?php

namespace App\Http\ApiV1\Modules\Stores\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateOrReplaceStorePickupTimeRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'store_id' => ['required', 'integer'],
            'day' => ['required', 'integer'],
            'pickup_time_code' => ['nullable', 'string'],
            'pickup_time_start' => ['nullable', 'string'],
            'pickup_time_end' => ['nullable', 'string'],
            'cargo_export_time' => ['nullable', 'string'],
            'delivery_service' => ['nullable', 'integer'],
      ];
    }
}
