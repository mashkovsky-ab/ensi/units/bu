<?php

namespace App\Domain\SellerUsers\Actions;

use App\Domain\SellerUsers\Models\Operator;
use Illuminate\Support\Arr;

class PatchOperatorAction
{
    public function execute(int $operatorId, array $fields): Operator
    {
        $operator = Operator::findOrFail($operatorId);
        $operator->update(Arr::only($fields, Operator::FILLABLE));

        return $operator;
    }
}
