<?php

namespace Database\Factories\Stores;

use App\Domain\Sellers\Models\Seller;
use App\Domain\Stores\Models\Store;
use Illuminate\Database\Eloquent\Factories\Factory;

class StoreFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Store::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $sellerIds = Seller::all()->pluck('id')->all();

        return [
            'seller_id' => $this->faker->randomElement($sellerIds),
            'xml_id' => $this->faker->unique()->word,
            'active' => $this->faker->boolean,
            'name' => $this->faker->name,
            'address' => [
                'address_string' => $this->faker->address,
                'country_code' => $this->faker->countryCode,
                'post_index' => $this->faker->postcode,
                'region' => $this->faker->region,
                'region_guid' => $this->faker->unique()->numberBetween(1, 10000000),
                'area' => $this->faker->region,
                'area_guid' => $this->faker->unique()->numberBetween(1, 10000000),
                'city' => $this->faker->city,
                'city_guid' => $this->faker->unique()->numberBetween(1, 10000000),
                'street' => $this->faker->streetName,
                'house' => $this->faker->numberBetween(1, 100),
                'block' => $this->faker->numberBetween(1, 100),
                'flat' => $this->faker->numberBetween(1, 100),
                'porch' => $this->faker->numberBetween(1, 100),
                'floor' => $this->faker->numberBetween(1, 20),
                'intercom' => $this->faker->numerify('k####c####'),
                'comment' => $this->faker->text,
                'geo_lat' => $this->faker->latitude,
                'geo_lon' => $this->faker->longitude,
            ],
            'timezone' => $this->faker->timezone,
        ];
    }
}
