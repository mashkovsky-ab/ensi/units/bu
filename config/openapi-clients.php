<?php

return [
    'units' => [
        'admin-auth' => [
            'base_uri' => env('ADMIN_AUTH_SERVICE_HOST') . '/api/v1',
        ],
    ],
];
