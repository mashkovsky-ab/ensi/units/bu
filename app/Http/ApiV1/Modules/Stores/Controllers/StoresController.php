<?php

namespace App\Http\ApiV1\Modules\Stores\Controllers;

use App\Domain\Stores\Actions\CreateStoreAction;
use App\Domain\Stores\Actions\DeleteStoreAction;
use App\Domain\Stores\Actions\PatchStoreAction;
use App\Domain\Stores\Actions\ReplaceStoreAction;
use App\Http\ApiV1\Modules\Stores\Queries\StoresQuery;
use App\Http\ApiV1\Modules\Stores\Requests\CreateOrReplaceStoreRequest;
use App\Http\ApiV1\Modules\Stores\Requests\PatchStoreRequest;
use App\Http\ApiV1\Modules\Stores\Resources\StoresResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class StoresController
{
    public function create(CreateOrReplaceStoreRequest $request, CreateStoreAction $action)
    {
        return new StoresResource($action->execute($request->validated()));
    }

    public function get(int $storeId, StoresQuery $query)
    {
        return new StoresResource($query->findOrFail($storeId));
    }

    public function replace(int $storeId, CreateOrReplaceStoreRequest $request, ReplaceStoreAction $action)
    {
        return new StoresResource($action->execute($storeId, $request->validated()));
    }

    public function patch(int $storeId, PatchStoreRequest $request, PatchStoreAction $action)
    {
        return new StoresResource($action->execute($storeId, $request->validated()));
    }

    public function delete(int $storeId, DeleteStoreAction $action)
    {
        $action->execute($storeId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, StoresQuery $query)
    {
        return StoresResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(StoresQuery $query)
    {
        return new StoresResource($query->firstOrFail());
    }
}
