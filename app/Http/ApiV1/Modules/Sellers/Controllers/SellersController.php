<?php

namespace App\Http\ApiV1\Modules\Sellers\Controllers;

use App\Domain\Sellers\Actions\CreateSellerAction;
use App\Domain\Sellers\Actions\CreateWithOperatorSellerAction;
use App\Domain\Sellers\Actions\DeleteSellerAction;
use App\Domain\Sellers\Actions\PatchSellerAction;
use App\Domain\Sellers\Actions\RegisterSellerAction;
use App\Domain\Sellers\Actions\ReplaceSellerAction;
use App\Domain\Sellers\Data\SellerStatus;
use App\Http\ApiV1\Modules\Sellers\Queries\SellersQuery;
use App\Http\ApiV1\Modules\Sellers\Requests\CreateOrReplaceSellerRequest;
use App\Http\ApiV1\Modules\Sellers\Requests\CreateWithOperatorOrRegisterSellerRequest;
use App\Http\ApiV1\Modules\Sellers\Requests\PatchSellerRequest;
use App\Http\ApiV1\Modules\Sellers\Resources\SellersResource;
use App\Http\ApiV1\Modules\Sellers\Resources\SellerStatusesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class SellersController
{
    public function create(CreateOrReplaceSellerRequest $request, CreateSellerAction $action)
    {
        return new SellersResource($action->execute($request->validated()));
    }

    public function get(int $sellerId, SellersQuery $query)
    {
        return new SellersResource($query->findOrFail($sellerId));
    }

    public function replace(int $sellerId, CreateOrReplaceSellerRequest $request, ReplaceSellerAction $action)
    {
        return new SellersResource($action->execute($sellerId, $request->validated()));
    }

    public function patch(int $sellerId, PatchSellerRequest $request, PatchSellerAction $action)
    {
        return new SellersResource($action->execute($sellerId, $request->validated()));
    }

    public function delete(int $sellerId, DeleteSellerAction $action)
    {
        $action->execute($sellerId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, SellersQuery $query)
    {
        return SellersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function createWithOperator(CreateWithOperatorOrRegisterSellerRequest $request, CreateWithOperatorSellerAction $action)
    {
        return new SellersResource($action->execute($request->validated()));
    }

    public function register(CreateWithOperatorOrRegisterSellerRequest $request, RegisterSellerAction $action)
    {
        return new SellersResource($action->execute($request->validated()));
    }

    public function getSellerStatuses()
    {
        return SellerStatusesResource::collection(SellerStatus::all());
    }
}
