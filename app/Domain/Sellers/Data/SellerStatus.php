<?php

namespace App\Domain\Sellers\Data;

use App\Http\ApiV1\OpenApiGenerated\Enums\SellerStatusEnum;

class SellerStatus
{
    public string $name;

    public function __construct(public int $id)
    {
        $this->fillNameById();
    }

    protected function fillNameById()
    {
        $this->name = match ($this->id) {
            SellerStatusEnum::CREATED => 'Продавец недооформлен, не дозаполнил информацию',
            SellerStatusEnum::ACTIVATE => 'Продавец активен',
            SellerStatusEnum::LOCKED => 'Продавец заблокирован',
        };
    }

    public static function all(): array
    {
        $all = [];
        foreach (SellerStatusEnum::cases() as $status) {
            $all[] = new static($status);
        }

        return $all;
    }
}
