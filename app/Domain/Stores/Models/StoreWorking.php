<?php

namespace App\Domain\Stores\Models;

use Carbon\Carbon;
use Database\Factories\Stores\StoreWorkingFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Класс-модель для сущности "Склады - время работы"
 * Class StoreWorking
 *
 * @property int $id
 * @property int $store_id - id склада
 * @property bool $active - флаг активности дня работы склада
 * @property int $day - день недели (1-7)
 * @property string $working_start_time - время начала работы склада (00:00)
 * @property string $working_end_time - время конца работы склада (00:00)
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class StoreWorking extends Model
{
    use HasFactory;

    /**
     * Заполняемые поля модели
     */
    const FILLABLE = [
        'store_id',
        'active',
        'day',
        'working_start_time',
        'working_end_time',
    ];

    protected $fillable = self::FILLABLE;

    protected $table = 'store_workings';

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return StoreWorkingFactory::new();
    }
}
