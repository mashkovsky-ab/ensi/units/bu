<?php

namespace Database\Factories\SellerUsers;

use App\Domain\Sellers\Models\Seller;
use App\Domain\SellerUsers\Models\Operator;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\Dto\AddRolesToUserRequest;
use Ensi\AdminAuthClient\Dto\CreateUserRequest;
use Ensi\AdminAuthClient\Dto\RoleEnum;
use Illuminate\Database\Eloquent\Factories\Factory;

class OperatorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Operator::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $sellerIds = Seller::all()->pluck('id')->all();

        $userId = null;
        $usersApi = resolve(UsersApi::class);
        while (!isset($userId)) {
            $operatorUserData = [
                'login' => $this->faker->unique()->userName,
                'password' => '123qwe',
                'first_name' => $this->faker->firstName,
                'middle_name' => $this->faker->firstName,
                'last_name' => $this->faker->lastName,
                'email' => $this->faker->email,
                'phone' => $this->faker->numerify('+7##########'),
                'active' => true,
            ];
            $createUserRequest = new CreateUserRequest($operatorUserData);
            $userId = $usersApi->createUser($createUserRequest)->getData()->getId();
        }
        $usersApi->addRolesToUser(
            $userId,
            new AddRolesToUserRequest([ 'roles' => [ RoleEnum::MAS_SELLER_ADMIN ]])
        );

        return [
            'seller_id' => $this->faker->randomElement($sellerIds),
            'user_id' => $userId,
            'is_receive_sms' => $this->faker->boolean,
            'is_main' => $this->faker->boolean,
        ];
    }
}
