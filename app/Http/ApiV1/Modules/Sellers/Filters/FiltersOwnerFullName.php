<?php

namespace App\Http\ApiV1\Modules\Sellers\Filters;

use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\Dto\SearchUsersRequest;
use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Filters\Filter;

class FiltersOwnerFullName implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $userIds = [];
        $usersApi = resolve(UsersApi::class);
        $request = new SearchUsersRequest([
            'filter' => [
                'full_name' => $value
            ],
            'pagination' => [
                'limit' => -1
            ]
        ]);
        $users = $usersApi->searchUsers($request)->getData();
        foreach ($users as $user) {
            $userIds[] = $user->getId();
        }

        $query->whereHas('operators', function (Builder $query) use ($userIds) {
            $query->whereIn('user_id', $userIds)
                ->where('is_main', true);
        });
    }
}
