<?php

namespace App\Domain\Sellers\Models;

use App\Domain\SellerUsers\Models\Operator;
use App\Domain\Stores\Models\Store;
use Carbon\Carbon;
use Database\Factories\Sellers\SellerFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * Класс-модель для сущности "Продавец"
 *
 * @property int $id - идентификатор продавца
 * @property string $legal_name - Юридическое наименование организации
 * @property string $external_id - Внешний код
 * @property string $legal_address - Юр. адрес
 * @property string $fact_address Фактический адрес
 * @property string $inn - ИНН
 * @property string $kpp - КПП
 * @property string $payment_account - Номер банковского счета
 * @property string $bank - Наименование банка
 * @property string $bank_address - юридический адрем банка
 * @property string $bank_bik - БИК банка
 * @property string $correspondent_account - номер корреспондентского счета банка
 * @property int $status - статус
 * @property Carbon $status_at - дата смены статуса
 * @property int $manager_id - ид менеджера
 * @property string $storage_address - Адреса складов отгрузки
 * @property string $site - Сайт компании
 * @property bool $can_integration - Подтверждение о возможности работы с Платформой с использованием автоматических механизмов интеграции
 * @property string $sale_info - Бренды и товары, которыми торгует Продавец
 * @property string $city - Город продавца
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read Collection|Operator[]|null $operators - менеджеры продавца
 * @property-read Collection|Store[]|null $stores - склады
 */
class Seller extends Model
{
    use HasFactory;

    /**
     * Заполняемые поля модели
     */
    public const FILLABLE = [
        'legal_name', 'external_id', 'legal_address', 'fact_address', 'inn', 'kpp', 'payment_account', 'bank',
        'bank_address', 'bank_bik', 'correspondent_account', 'status', 'manager_id', 'storage_address', 'site',
        'can_integration', 'sale_info', 'city',
    ];

    protected $fillable = self::FILLABLE;

    protected $table = 'sellers';

    protected $dates = ['status_at'];

    /**
     * @return HasMany
     */
    public function operators(): HasMany
    {
        return $this->hasMany(Operator::class);
    }

    /**
     * @return HasMany
     */
    public function stores(): HasMany
    {
        return $this->hasMany(Store::class);
    }

    public function scopeCreatedAtFrom(Builder $query, $date): Builder
    {
        return $query->where('created_at', '>=', $date);
    }

    public function scopeCreatedAtTo(Builder $query, $date): Builder
    {
        return $query->where('created_at', '<=', $date);
    }

    protected static function boot()
    {
        parent::boot();

        static::saving(function (Seller $seller) {
            if ($seller->status != $seller->getOriginal('status')) {
                $seller->status_at = now();
            }
        });
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return SellerFactory::new();
    }
}
