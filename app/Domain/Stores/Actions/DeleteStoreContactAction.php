<?php

namespace App\Domain\Stores\Actions;

use App\Domain\Stores\Models\StoreContact;

class DeleteStoreContactAction
{
    public function execute(int $storeContactId): void
    {
        StoreContact::destroy($storeContactId);
    }
}
