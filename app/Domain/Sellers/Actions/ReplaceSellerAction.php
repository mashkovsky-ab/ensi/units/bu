<?php

namespace App\Domain\Sellers\Actions;

use App\Domain\Sellers\Models\Seller;
use Illuminate\Support\Arr;

class ReplaceSellerAction
{
    public function execute(int $sellerId, array $fields): Seller
    {
        $seller = Seller::findOrFail($sellerId);

        $newAttributes = Arr::only($fields, Seller::FILLABLE);
        foreach (Seller::FILLABLE as $attributeKey) {
            if (!array_key_exists($attributeKey, $newAttributes)) {
                $newAttributes[$attributeKey] = null;
            }
        }
        $seller->update($newAttributes);

        return $seller;
    }
}
