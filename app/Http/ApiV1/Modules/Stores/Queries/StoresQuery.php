<?php

namespace App\Http\ApiV1\Modules\Stores\Queries;

use App\Domain\Stores\Models\Store;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedInclude;
use Spatie\QueryBuilder\QueryBuilder;

class StoresQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = Store::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedIncludes(['workings', 'contacts', AllowedInclude::relationship('pickup_times', 'pickupTimes'), 'contact',]);

        $this->allowedSorts([
            'id',
            'seller_id',
            'xml_id',
            'active',
            'name',
//            'address',
            'timezone',
            'created_at',
            'updated_at',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('seller_id'),
            AllowedFilter::exact('xml_id'),
            AllowedFilter::exact('active'),
            AllowedFilter::exact('name'),
//            AllowedFilter::exact('address'),
            AllowedFilter::exact('timezone'),
            AllowedFilter::scope('address_string', null, ';'),
            AllowedFilter::exact('contact_name', 'contacts.name'),
            AllowedFilter::exact('contact_phone', 'contacts.phone'),
        ]);

        $this->defaultSort('id');
    }
}
