<?php

namespace App\Http\ApiV1\Modules\Stores\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class StorePickupTimesResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'store_id' => $this->store_id,
            'day' => $this->day,
            'pickup_time_code' => $this->pickup_time_code,
            'pickup_time_start' => $this->pickup_time_start,
            'pickup_time_end' => $this->pickup_time_end,
            'cargo_export_time' => $this->cargo_export_time,
            'delivery_service' => $this->delivery_service,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
