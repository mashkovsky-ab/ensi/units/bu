<?php

namespace App\Domain\Stores\Actions;

use App\Domain\Stores\Models\StorePickupTime;

class DeleteStorePickupTimeAction
{
    public function execute(int $storePickupTimeId): void
    {
        StorePickupTime::destroy($storePickupTimeId);
    }
}
