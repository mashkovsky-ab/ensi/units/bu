<?php

namespace App\Domain\Stores\Actions;

use App\Domain\Stores\Models\Store;
use Illuminate\Support\Arr;

class PatchStoreAction
{
    public function execute(int $storeId, array $fields): Store
    {
        $store = Store::findOrFail($storeId);
        $store->update(Arr::only($fields, Store::FILLABLE));

        return $store;
    }
}
