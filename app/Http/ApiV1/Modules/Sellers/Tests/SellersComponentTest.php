<?php

use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/sellers/sellers:create 400', function () {
    postJson('/api/v1/sellers/sellers:create')
        ->assertStatus(400);
});

test('POST /api/v1/sellers/sellers:register 400', function () {
    postJson('/api/v1/sellers/sellers:register')
        ->assertStatus(400);
});

test('GET /api/v1/sellers/seller-statuses 200', function () {
    getJson('/api/v1/sellers/seller-statuses')
        ->assertStatus(200);
});
