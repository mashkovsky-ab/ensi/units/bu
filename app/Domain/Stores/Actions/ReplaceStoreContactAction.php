<?php

namespace App\Domain\Stores\Actions;

use App\Domain\Stores\Models\StoreContact;
use Illuminate\Support\Arr;

class ReplaceStoreContactAction
{
    public function execute(int $storeContactId, array $fields): StoreContact
    {
        $storeContact = StoreContact::findOrFail($storeContactId);

        $newAttributes = Arr::only($fields, StoreContact::FILLABLE);
        foreach (StoreContact::FILLABLE as $attributeKey) {
            if (!array_key_exists($attributeKey, $newAttributes)) {
                $newAttributes[$attributeKey] = null;
            }
        }
        $storeContact->update($newAttributes);

        return $storeContact;
    }
}
