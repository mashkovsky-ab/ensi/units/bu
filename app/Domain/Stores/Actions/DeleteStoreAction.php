<?php

namespace App\Domain\Stores\Actions;

use App\Domain\Stores\Models\Store;

class DeleteStoreAction
{
    public function execute(int $storeId): void
    {
        Store::destroy($storeId);
    }
}
