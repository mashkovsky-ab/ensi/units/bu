<?php

namespace App\Http\ApiV1\Modules\Stores\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchStoreWorkingRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'store_id' => ['nullable', 'integer'],
            'active' => ['nullable', 'bool'],
            'day' => ['nullable', 'int'],
            'working_start_time' => ['nullable', 'string'],
            'working_end_time' => ['nullable', 'string'],
      ];
    }
}
