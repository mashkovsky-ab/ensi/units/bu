<?php

namespace App\Domain\Stores\Actions;

use App\Domain\Stores\Models\Store;
use Illuminate\Support\Arr;

class ReplaceStoreAction
{
    public function execute(int $storeId, array $fields): Store
    {
        $store = Store::findOrFail($storeId);

        $newAttributes = Arr::only($fields, Store::FILLABLE);
        foreach (Store::FILLABLE as $attributeKey) {
            if (!array_key_exists($attributeKey, $newAttributes)) {
                $newAttributes[$attributeKey] = null;
            }
        }
        $store->update($newAttributes);

        return $store;
    }
}
