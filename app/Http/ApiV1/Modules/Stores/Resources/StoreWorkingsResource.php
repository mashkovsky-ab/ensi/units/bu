<?php

namespace App\Http\ApiV1\Modules\Stores\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class StoreWorkingsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'store_id' => $this->store_id,
            'active' => $this->active,
            'day' => $this->day,
            'working_start_time' => $this->working_start_time,
            'working_end_time' => $this->working_end_time,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
