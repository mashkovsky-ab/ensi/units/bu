<?php

namespace App\Domain\Sellers\Actions;

use App\Domain\Sellers\Models\Seller;

class DeleteSellerAction
{
    public function execute(int $sellerId): void
    {
        Seller::destroy($sellerId);
    }
}
