<?php

namespace App\Domain\Stores\Actions;

use App\Domain\Stores\Models\StorePickupTime;
use Illuminate\Support\Arr;

class ReplaceStorePickupTimeAction
{
    public function execute(int $storePickupTimeId, array $fields): StorePickupTime
    {
        $storePickupTime = StorePickupTime::findOrFail($storePickupTimeId);

        $newAttributes = Arr::only($fields, StorePickupTime::FILLABLE);
        foreach (StorePickupTime::FILLABLE as $attributeKey) {
            if (!array_key_exists($attributeKey, $newAttributes)) {
                $newAttributes[$attributeKey] = null;
            }
        }
        $storePickupTime->update($newAttributes);

        return $storePickupTime;
    }
}
