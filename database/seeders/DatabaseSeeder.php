<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SellerSeeder::class);
        $this->call(OperatorSeeder::class);
        $this->call(StoreSeeder::class);
        $this->call(StoreContactSeeder::class);
        $this->call(StoreWorkingSeeder::class);
        $this->call(StorePickupTimeSeeder::class);
    }
}
