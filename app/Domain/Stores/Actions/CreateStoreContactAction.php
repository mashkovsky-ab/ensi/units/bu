<?php

namespace App\Domain\Stores\Actions;

use App\Domain\Stores\Models\StoreContact;
use Illuminate\Support\Arr;

class CreateStoreContactAction
{
    public function execute(array $fields): StoreContact
    {
        return StoreContact::create(Arr::only($fields, StoreContact::FILLABLE));
    }
}
