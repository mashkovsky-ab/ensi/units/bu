<?php

namespace App\Domain\Stores\Actions;

use App\Domain\Stores\Models\StoreContact;
use Illuminate\Support\Arr;

class PatchStoreContactAction
{
    public function execute(int $storeContactId, array $fields): StoreContact
    {
        $storeContact = StoreContact::findOrFail($storeContactId);
        $storeContact->update(Arr::only($fields, StoreContact::FILLABLE));

        return $storeContact;
    }
}
