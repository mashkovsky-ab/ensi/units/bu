<?php

namespace App\Http\ApiV1\Modules\SellerUsers\Resources;

use App\Http\ApiV1\Modules\Sellers\Resources\SellersResource;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class OperatorsResource extends BaseJsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'seller_id' => $this->seller_id,
            'user_id' => $this->user_id,
            'is_receive_sms' => $this->is_receive_sms,
            'is_main' => $this->is_main,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'seller' => SellersResource::make($this->whenLoaded('seller')),
        ];
    }
}
