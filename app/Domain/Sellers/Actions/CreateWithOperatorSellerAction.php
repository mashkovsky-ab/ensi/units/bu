<?php

namespace App\Domain\Sellers\Actions;

use App\Domain\Sellers\Models\Seller;

class CreateWithOperatorSellerAction
{
    private NewSellerWithOperatorAction $newSellerWithOperatorAction;

    public function __construct(NewSellerWithOperatorAction $newSellerWithOperatorAction)
    {
        $this->newSellerWithOperatorAction = $newSellerWithOperatorAction;
    }

    public function execute(array $fields): Seller
    {
        return $this->newSellerWithOperatorAction->execute($fields, Seller::STATUS_ACTIVATION);
    }
}
