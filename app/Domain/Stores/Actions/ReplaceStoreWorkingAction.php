<?php

namespace App\Domain\Stores\Actions;

use App\Domain\Stores\Models\StoreWorking;
use Illuminate\Support\Arr;

class ReplaceStoreWorkingAction
{
    public function execute(int $storeWorkingId, array $fields): StoreWorking
    {
        $storeWorking = StoreWorking::findOrFail($storeWorkingId);

        $newAttributes = Arr::only($fields, StoreWorking::FILLABLE);
        foreach (StoreWorking::FILLABLE as $attributeKey) {
            if (!array_key_exists($attributeKey, $newAttributes)) {
                $newAttributes[$attributeKey] = null;
            }
        }
        $storeWorking->update($newAttributes);

        return $storeWorking;
    }
}
