<?php

namespace App\Http\ApiV1\Modules\Sellers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateWithOperatorOrRegisterSellerRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            // Информация продавца
            'seller' => ['required', 'array'],
            'seller.legal_name' => ['required', 'string'],
            'seller.external_id' => ['required', 'string'],
            'seller.inn' => ['nullable', 'string'],
            'seller.kpp' => ['nullable', 'string'],
            'seller.legal_address' => ['nullable', 'string'],
            'seller.fact_address' => ['nullable', 'string'],
            'seller.payment_account' => ['nullable', 'string'],
            'seller.bank' => ['nullable', 'string'],
            'seller.bank_address' => ['nullable', 'string'],
            'seller.bank_bik' => ['nullable', 'string'],
            'seller.status' => ['nullable', 'integer'],
            'seller.manager_id' => ['nullable', 'integer'],
            'seller.correspondent_account' => ['nullable', 'string'],
            'seller.storage_address' => ['nullable', 'string'],
            'seller.site' => ['nullable', 'string'],
            'seller.can_integration' => ['nullable', 'boolean'],
            'seller.sale_info' => ['nullable', 'string'],
            'seller.city' => ['nullable', 'string'],

            // Информация оператора
            'operator' => ['required', 'array'],
            'operator.first_name' => ['nullable', 'string'],
            'operator.last_name' => ['nullable', 'string'],
            'operator.middle_name' => ['nullable', 'string'],
            'operator.email' => ['nullable', 'email'],
            'operator.phone' => ['nullable', 'regex:/^\+7\d{10}$/'],
            'operator.password' => ['nullable', 'string', 'min:8'],
        ];
    }
}
