<?php

namespace App\Domain\SellerUsers\Actions;

use App\Domain\SellerUsers\Models\Operator;
use Illuminate\Support\Arr;

class CreateOperatorAction
{
    public function execute(array $fields): Operator
    {
        return Operator::create(Arr::only($fields, Operator::FILLABLE));
    }
}
