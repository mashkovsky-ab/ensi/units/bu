<?php

namespace App\Http\ApiV1\Modules\SellerUsers\Controllers;

use App\Domain\SellerUsers\Actions\CreateOperatorAction;
use App\Domain\SellerUsers\Actions\DeleteOperatorAction;
use App\Domain\SellerUsers\Actions\PatchOperatorAction;
use App\Domain\SellerUsers\Actions\ReplaceOperatorAction;
use App\Http\ApiV1\Modules\SellerUsers\Queries\OperatorsQuery;
use App\Http\ApiV1\Modules\SellerUsers\Requests\CreateOrReplaceOperatorRequest;
use App\Http\ApiV1\Modules\SellerUsers\Requests\PatchOperatorRequest;
use App\Http\ApiV1\Modules\SellerUsers\Resources\OperatorsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class OperatorsController
{
    public function create(CreateOrReplaceOperatorRequest $request, CreateOperatorAction $action)
    {
        return new OperatorsResource($action->execute($request->validated()));
    }

    public function get(int $operatorId, OperatorsQuery $query)
    {
        return new OperatorsResource($query->findOrFail($operatorId));
    }

    public function replace(int $operatorId, CreateOrReplaceOperatorRequest $request, ReplaceOperatorAction $action)
    {
        return new OperatorsResource($action->execute($operatorId, $request->validated()));
    }

    public function patch(int $operatorId, PatchOperatorRequest $request, PatchOperatorAction $action)
    {
        return new OperatorsResource($action->execute($operatorId, $request->validated()));
    }

    public function delete(int $operatorId, DeleteOperatorAction $action)
    {
        $action->execute($operatorId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, OperatorsQuery $query)
    {
        return OperatorsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
