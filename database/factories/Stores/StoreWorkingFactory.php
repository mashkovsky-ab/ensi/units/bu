<?php

namespace Database\Factories\Stores;

use App\Domain\Stores\Models\Store;
use App\Domain\Stores\Models\StoreWorking;
use Illuminate\Database\Eloquent\Factories\Factory;

class StoreWorkingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StoreWorking::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $storeIds = Store::all()->pluck('id')->all();

        return [
            'store_id' => $this->faker->randomElement($storeIds),
            'active' => $this->faker->boolean,
            'day' => $this->faker->numberBetween(1, 7),
            'working_start_time' => $this->faker->time('H:i'),
            'working_end_time' => $this->faker->time('H:i'),
        ];
    }
}
