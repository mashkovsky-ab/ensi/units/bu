<?php

namespace App\Http\ApiV1\Modules\SellerUsers\Queries;

use App\Domain\SellerUsers\Models\Operator;
use App\Http\ApiV1\Modules\SellerUsers\Filters\FiltersUserActive;
use App\Http\ApiV1\Modules\SellerUsers\Filters\FiltersUserEmail;
use App\Http\ApiV1\Modules\SellerUsers\Filters\FiltersUserFullName;
use App\Http\ApiV1\Modules\SellerUsers\Filters\FiltersUserLogin;
use App\Http\ApiV1\Modules\SellerUsers\Filters\FiltersUserPhone;
use App\Http\ApiV1\Modules\SellerUsers\Filters\FiltersUserRole;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class OperatorsQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = Operator::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedIncludes(['seller']);

        $this->allowedSorts([
            'id',
            'seller_id',
            'user_id',
            'is_receive_sms',
            'is_main',
            'created_at',
            'updated_at',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('seller_id'),
            AllowedFilter::exact('user_id'),
            AllowedFilter::exact('is_receive_sms'),
            AllowedFilter::exact('is_main'),
            AllowedFilter::custom('email', new FiltersUserEmail()),
            AllowedFilter::custom('full_name', new FiltersUserFullName()),
            AllowedFilter::custom('phone', new FiltersUserPhone()),
            AllowedFilter::custom('login', new FiltersUserLogin()),
            AllowedFilter::custom('active', new FiltersUserActive()),
            AllowedFilter::custom('role', new FiltersUserRole()),
        ]);

        $this->defaultSort('id');
    }
}
