<?php

namespace App\Http\ApiV1\Modules\Sellers\Resources;

use App\Domain\sellers\Data\SellerStatus;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * Class OrderStatusesResource
 * @package App\Http\ApiV1\Modules\Sellers\Resources
 * @mixin SellerStatus
 */
class SellerStatusesResource extends BaseJsonResource
{
    /**
     * @inheritDoc
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
