<?php

namespace App\Http\ApiV1\Modules\SellerUsers\Filters;

use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\Dto\SearchUsersRequest;
use Illuminate\Database\Eloquent\Builder;
use Spatie\QueryBuilder\Filters\Filter;

class FiltersUserEmail implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $userIds = [];
        $usersApi = resolve(UsersApi::class);
        $request = new SearchUsersRequest([
            'filter' => [
                'email' => $value
            ]
        ]);
        $users = $usersApi->searchUsers($request)->getData();
        foreach ($users as $user) {
            $userIds[] = $user->getId();
        }
        $query->whereIn('user_id', $userIds);
    }
}
