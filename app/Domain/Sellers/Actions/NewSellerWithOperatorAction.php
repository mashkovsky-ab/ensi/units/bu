<?php

namespace App\Domain\Sellers\Actions;

use App\Domain\Sellers\Models\Seller;
use App\Domain\SellerUsers\Actions\CreateOperatorAction;
use App\Domain\SellerUsers\Models\Operator;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\Dto\AddRolesToUserRequest;
use Ensi\AdminAuthClient\Dto\CreateUserRequest;
use Ensi\AdminAuthClient\Dto\RoleEnum;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class NewSellerWithOperatorAction
{
    private UsersApi $usersApi;
    private CreateSellerAction $createSellerAction;
    private CreateOperatorAction $createOperatorAction;

    public function __construct(UsersApi $usersApi, CreateSellerAction $createSellerAction, CreateOperatorAction $createOperatorAction)
    {
        $this->usersApi = $usersApi;
        $this->createSellerAction = $createSellerAction;
        $this->createOperatorAction = $createOperatorAction;
    }

    public function execute(array $fields, int $status): Seller
    {
        $sellerData = data_get($fields, 'seller');
        $operatorUserData = data_get($fields, 'operator');

        if (isset($operatorUserData['email'])) {
            $createUserRequest = new CreateUserRequest($operatorUserData);
            $createUserRequest->setLogin($operatorUserData['email']);
            $userId = $this->usersApi->createUser($createUserRequest)->getData()->getId();
            if (!isset($userId)) {
                throw new Exception('Возникли проблемы при создании оператора (пользователя)');
            }
            $this->usersApi->addRolesToUser(
                $userId,
                new AddRolesToUserRequest([ 'roles' => [ RoleEnum::MAS_SELLER_ADMIN ] ])
            );
        } else {
            throw new Exception('Для создания продавца необходимо указать email оператора');
        }

        $seller = null;
        DB::transaction(function () use ($sellerData, $status, $userId, &$seller) {
            $sellerData['status'] = $status;
            $sellerData['can_integration'] = true;
            $seller = $this->createSellerAction->execute(Arr::only($sellerData, Seller::FILLABLE));

            $operatorData['seller_id'] = $seller->id;
            $operatorData['user_id'] = $userId;
            $operatorData['is_main'] = true;
            $this->createOperatorAction->execute(Arr::only($operatorData, Operator::FILLABLE));
        });

        return $seller->load('operators');
    }
}
