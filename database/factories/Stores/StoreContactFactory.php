<?php

namespace Database\Factories\Stores;

use App\Domain\Stores\Models\Store;
use App\Domain\Stores\Models\StoreContact;
use Illuminate\Database\Eloquent\Factories\Factory;

class StoreContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StoreContact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $storeIds = Store::all()->pluck('id')->all();

        return [
            'store_id' => $this->faker->randomElement($storeIds),
            'name' => "{$this->faker->lastName} {$this->faker->firstName} {$this->faker->firstName}",
            'phone' => $this->faker->numerify('+7##########'),
            'email' => $this->faker->email,
        ];
    }
}
