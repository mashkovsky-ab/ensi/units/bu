<?php

namespace App\Domain\Stores\Actions;

use App\Domain\Stores\Models\StoreWorking;
use Illuminate\Support\Arr;

class CreateStoreWorkingAction
{
    public function execute(array $fields): StoreWorking
    {
        return StoreWorking::create(Arr::only($fields, StoreWorking::FILLABLE));
    }
}
