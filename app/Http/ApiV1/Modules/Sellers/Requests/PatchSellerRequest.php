<?php

namespace App\Http\ApiV1\Modules\Sellers\Requests;

use App\Http\ApiV1\OpenApiGenerated\Enums\SellerStatusEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchSellerRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'legal_name' => ['nullable', 'string'],
            'external_id' => ['nullable', 'string'],
            'legal_address' => ['nullable', 'string'],
            'fact_address'  => ['nullable', 'string'],
            'inn' => ['nullable', 'string'],
            'kpp' => ['nullable', 'string'],
            'payment_account' => ['nullable', 'string'],
            'bank' => ['nullable', 'string'],
            'bank_address' => ['nullable', 'string'],
            'bank_bik' => ['nullable', 'string'],
            'correspondent_account' => ['nullable', 'string'],
            'status' => ['nullable', 'integer', Rule::in(SellerStatusEnum::cases())],
            'manager_id' => ['nullable', 'integer'],
            'storage_address' => ['nullable', 'string'],
            'site' => ['nullable', 'string'],
            'can_integration' => ['nullable', 'boolean'],
            'sale_info' => ['nullable', 'string'],
            'city'  => ['nullable', 'string'],
      ];
    }
}
