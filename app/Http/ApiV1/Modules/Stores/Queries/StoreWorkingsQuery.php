<?php

namespace App\Http\ApiV1\Modules\Stores\Queries;

use App\Domain\Stores\Models\StoreWorking;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class StoreWorkingsQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = StoreWorking::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedIncludes([]);

        $this->allowedSorts([
            'id',
            'store_id',
            'active',
            'working_start_time',
            'working_end_time',
            'created_at',
            'updated_at',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('store_id'),
            AllowedFilter::exact('active'),
            AllowedFilter::exact('working_start_time'),
            AllowedFilter::exact('working_end_time'),
        ]);

        $this->defaultSort('id');
    }
}
