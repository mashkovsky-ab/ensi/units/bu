<?php

namespace Database\Factories\Stores;

use App\Domain\Stores\Models\Store;
use App\Domain\Stores\Models\StorePickupTime;
use Illuminate\Database\Eloquent\Factories\Factory;

class StorePickupTimeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StorePickupTime::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $storeIds = Store::all()->pluck('id')->all();

        return [
            'store_id' => $this->faker->randomElement($storeIds),
            'day' => $this->faker->numberBetween(1, 7),
            'pickup_time_code' => "{$this->faker->numberBetween(0, 23)}-{$this->faker->numberBetween(0, 23)}",
            'pickup_time_start' => $this->faker->time('H:i'),
            'pickup_time_end' => $this->faker->time('H:i'),
            'cargo_export_time' => $this->faker->time('H:i'),
            'delivery_service' => null,
        ];
    }
}
