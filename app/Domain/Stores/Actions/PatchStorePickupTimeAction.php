<?php

namespace App\Domain\Stores\Actions;

use App\Domain\Stores\Models\StorePickupTime;
use Illuminate\Support\Arr;

class PatchStorePickupTimeAction
{
    public function execute(int $storePickupTimeId, array $fields): StorePickupTime
    {
        $storePickupTime = StorePickupTime::findOrFail($storePickupTimeId);
        $storePickupTime->update(Arr::only($fields, StorePickupTime::FILLABLE));

        return $storePickupTime;
    }
}
