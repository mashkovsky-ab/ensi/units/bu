<?php

namespace App\Http\ApiV1\Modules\Sellers\Queries;

use App\Domain\Sellers\Models\Seller;
use App\Http\ApiV1\Modules\Sellers\Filters\FiltersOwnerEmail;
use App\Http\ApiV1\Modules\Sellers\Filters\FiltersOwnerFullName;
use App\Http\ApiV1\Modules\Sellers\Filters\FiltersOwnerPhone;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class SellersQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = Seller::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedIncludes(['operators', 'stores']);

        $this->allowedSorts([
            'id',
            'legal_name',
            'external_id',
            'status',
            'manager_id',
            'city',
            'created_at',
            'updated_at',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('legal_name'),
            AllowedFilter::exact('external_id'),
            AllowedFilter::exact('status'),
            AllowedFilter::exact('manager_id'),
            AllowedFilter::exact('city'),
            AllowedFilter::exact('manager_user_id', 'manager_id'),

            AllowedFilter::scope('created_at_from'),
            AllowedFilter::scope('created_at_to'),

            AllowedFilter::custom('owner_full_name', new FiltersOwnerFullName()),
            AllowedFilter::custom('owner_phone', new FiltersOwnerPhone()),
            AllowedFilter::custom('owner_email', new FiltersOwnerEmail()),

        ]);

        $this->defaultSort('id');
    }
}
