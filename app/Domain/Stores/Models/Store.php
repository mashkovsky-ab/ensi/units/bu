<?php

namespace App\Domain\Stores\Models;

use Carbon\Carbon;
use Database\Factories\Stores\StoreFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Класс-модель для сущности "Склады"
 * Class Store
 *
 * @property int $seller_id - id продавца
 * @property string $xml_id - id склада у продавца
 * @property bool $active - флаг активности склада
 * @property string $name - название
 * @property array $address - адрес
 *  - address_string - адрес одной строкой
 *  - country_code - код страны
 *  - post_index - почтовый индекс
 *  - region - регион
 *  - region_guid - ФИАС ID региона
 *  - area - район в регионе
 *  - area_guid - ФИАС ID района в регионе
 *  - city - город/населенный пункт
 *  - city_guid - ФИАС ID города/населенного пункта
 *  - street - улица
 *  - house - дом
 *  - block - строение/корпус
 *  - flat - квартира/офис
 *  - porch - подъезд
 *  - floor - этаж
 *  - intercom - домофон
 *  - comment - комментарий к адресу
 *  - geo_lat - координаты: широта
 *  - geo_lon - координаты: долгота
 * @property string $timezone - часовой пояс
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read Collection|StoreWorking[] $storeWorking
 * @property-read Collection|StoreContact[] $storeContact
 * @property-read Collection|StorePickupTime[] $storePickupTime
 */
class Store extends Model
{
    use HasFactory;

    /**
     * Заполняемые поля модели
     */
    const FILLABLE = ['seller_id', 'xml_id', 'active', 'name', 'address', 'timezone'];

    protected $casts = [
        'address' => 'array',
    ];

    protected $fillable = self::FILLABLE;

    /**
     * @return HasMany
     */
    public function workings(): HasMany
    {
        return $this->hasMany(StoreWorking::class, 'store_id');
    }

    /**
     * @return HasMany
     */
    public function contacts(): HasMany
    {
        return $this->hasMany(StoreContact::class, 'store_id');
    }

    /**
     * @return HasOne
     */
    public function contact(): HasOne
    {
        return $this->hasOne(StoreContact::class, 'store_id')->oldestOfMany();
    }

    /**
     * @return HasMany
     */
    public function pickupTimes(): HasMany
    {
        return $this->hasMany(StorePickupTime::class, 'store_id');
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return StoreFactory::new();
    }

    /**
     * @param Builder $query
     * @param string $value
     * @return Builder
     */
    public function scopeAddressString($query, $value)
    {
        return $query->where('address->address_string', 'like', "%$value%");
    }
}
