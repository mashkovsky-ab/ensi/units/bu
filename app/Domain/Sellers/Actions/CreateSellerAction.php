<?php

namespace App\Domain\Sellers\Actions;

use App\Domain\Sellers\Models\Seller;
use Illuminate\Support\Arr;

class CreateSellerAction
{
    public function execute(array $fields): Seller
    {
        return Seller::create(Arr::only($fields, Seller::FILLABLE));
    }
}
