<?php

namespace App\Http\ApiV1\Support;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Trait Params
 * @package App\Http\ApiV1\Support
 */
trait Params
{
    /**
     * Установить значение(я) для настройки
     * @return Response
     * @throws \Throwable
     */
    public function setValues(Request $request): Response
    {
        $name = request()->route('name');
        /** @var Model $modelClass */
        $modelClass = $this->modelClass();
        /** @var Model $foreignModelClass */
        $foreignModelClass = $this->foreignModelClass();
        $foreignKey = $this->foreignKey();

        // todo добавить проверку прав
        $values = (array)$request->input('values');
        $foreignId = $this->foreignId();
        $foreignEntity = $foreignModelClass::query()->find($foreignId);
        if (!$foreignEntity) {
            throw new NotFoundHttpException($this->foreignEntityNotFound());
        }

        $oldValues = $modelClass::query()
            ->where($foreignKey, $foreignId)
            ->where('name', $name)
            ->get();

        try {
            DB::transaction(function () use ($name, $values, $modelClass, $foreignEntity, $foreignKey, $oldValues) {
                if ($oldValues && $oldValues->count()) {
                    //Если значения настройки уже существует, то удаляем их из массива $values и коллекции $oldValues
                    foreach ($oldValues as $oldKey => $oldValue) {
                        if ($key = array_search($oldValue->value, $values)) {
                            unset($values[$key]);
                            $oldValues->forget($oldKey);
                        }
                    }

                    //Новые значения настройки записываем вместо существующих, сохраняя их id
                    if ($oldValues->count()) {
                        foreach ($oldValues as $oldValue) {
                            if ($values) {
                                $oldValue->value = array_shift($values);
                                $oldValue->save();
                            } else {
                                //Удаляем оставшиеся существующие
                                $oldValue->delete();
                            }
                        }
                    }
                }

                //Сохраняем оставшиеся новые значения
                if ($values) {
                    foreach ($values as $value) {
                        /** @var Model $model */
                        $model = new $modelClass([
                            $foreignKey => $foreignEntity->id,
                            'name' => $name,
                            'value' => $value,
                        ]);
                        $model->save();
                    }
                }
            });
        } catch (\Exception $e) {
            throw new HttpException(500, $e->getMessage());
        }

        return response('', 204);
    }

    /**
     * Получить значение(я) для настройки
     * @return Response
     */
    public function getValues(): JsonResponse
    {
        $name = request()->route('name');
        /** @var Model $modelClass */
        $modelClass = $this->modelClass();
        /** @var Model $foreignModelClass */
        $foreignModelClass = $this->foreignModelClass();
        $foreignKey = $this->foreignKey();

        $foreignId = $this->foreignId();
        $foreignEntity = $foreignModelClass::query()->find($foreignId);
        if (!$foreignEntity) {
            throw new NotFoundHttpException($this->foreignEntityNotFound());
        }

        $values = $modelClass::query()
            ->where($foreignKey, $foreignId)
            ->where('name', $name)
            ->get();

        return response()->json([
            'items' => $values,
        ]);
    }
}
