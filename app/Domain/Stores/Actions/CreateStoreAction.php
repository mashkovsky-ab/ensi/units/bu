<?php

namespace App\Domain\Stores\Actions;

use App\Domain\Stores\Models\Store;
use Illuminate\Support\Arr;

class CreateStoreAction
{
    public function execute(array $fields): Store
    {
        return Store::create(Arr::only($fields, Store::FILLABLE));
    }
}
