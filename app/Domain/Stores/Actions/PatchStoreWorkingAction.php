<?php

namespace App\Domain\Stores\Actions;

use App\Domain\Stores\Models\StoreWorking;
use Illuminate\Support\Arr;

class PatchStoreWorkingAction
{
    public function execute(int $storeWorkingId, array $fields): StoreWorking
    {
        $storeWorking = StoreWorking::findOrFail($storeWorkingId);
        $storeWorking->update(Arr::only($fields, StoreWorking::FILLABLE));

        return $storeWorking;
    }
}
