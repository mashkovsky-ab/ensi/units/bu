<?php

namespace App\Domain\Stores\Actions;

use App\Domain\Stores\Models\StorePickupTime;
use Illuminate\Support\Arr;

class CreateStorePickupTimeAction
{
    public function execute(array $fields): StorePickupTime
    {
        return StorePickupTime::create(Arr::only($fields, StorePickupTime::FILLABLE));
    }
}
