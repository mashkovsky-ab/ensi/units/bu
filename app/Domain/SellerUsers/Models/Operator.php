<?php

namespace App\Domain\SellerUsers\Models;

use App\Domain\Sellers\Models\Seller;
use Carbon\Carbon;
use Database\Factories\SellerUsers\OperatorFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Менеджер продавца"
 *
 * @property int $id - идентификатор оператора
 * @property int $seller_id
 * @property int $user_id
 * @property bool $is_receive_sms
 * @property bool $is_main - главное контактное лицо продавца
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property-read Seller $seller - продавец
 */
class Operator extends Model
{
    use HasFactory;

    /**
     * Заполняемые поля модели
     */
    const FILLABLE = ['seller_id', 'user_id', 'is_receive_sms', 'is_main'];

    protected $fillable = self::FILLABLE;

    protected $table = 'operators';

    /**
     * @return BelongsTo
     */
    public function seller(): BelongsTo
    {
        return $this->belongsTo(Seller::class);
    }

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return OperatorFactory::new();
    }
}
