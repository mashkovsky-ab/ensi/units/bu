<?php

namespace Database\Seeders;

use App\Domain\Stores\Models\StoreContact;
use Illuminate\Database\Seeder;

class StoreContactSeeder extends Seeder
{
    public function run()
    {
        StoreContact::factory()->count(20)->create();
    }
}
