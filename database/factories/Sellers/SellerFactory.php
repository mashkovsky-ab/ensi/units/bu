<?php

namespace Database\Factories\Sellers;

use App\Domain\Sellers\Models\Seller;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\Dto\RoleEnum;
use Ensi\AdminAuthClient\Dto\SearchUsersRequest;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class SellerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Seller::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $searchUsersRequest = new SearchUsersRequest();
        $searchUsersRequest->setFilter(['role' => RoleEnum::MANAGER_SELLER]);
        $usersApi = resolve(UsersApi::class);
        $userIds = Arr::pluck($usersApi->searchUsers($searchUsersRequest)->getData(), 'id');

        return [
            'legal_name' => $this->faker->unique()->company,
            'external_id' => $this->faker->unique()->word,
            'legal_address' => $this->faker->address,
            'fact_address' => $this->faker->address,
            'inn' => $this->faker->numerify('##########'),
            'kpp' => $this->faker->numerify('#########'),
            'payment_account' => $this->faker->numerify('####################'),
            'bank' => $this->faker->company,
            'bank_address' => $this->faker->address,
            'bank_bik' => $this->faker->numerify('#########'),
            'correspondent_account' => $this->faker->numerify('####################'),
            'status' => $this->faker->numberBetween(1, 8),
            'manager_id' => $this->faker->randomElement($userIds),
            'storage_address' => $this->faker->address,
            'site' => $this->faker->url,
            'can_integration' => $this->faker->boolean,
            'sale_info' => $this->faker->text,
            'city' => $this->faker->city,
        ];
    }
}
