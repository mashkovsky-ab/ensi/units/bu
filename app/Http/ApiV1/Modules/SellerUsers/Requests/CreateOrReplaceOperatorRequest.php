<?php

namespace App\Http\ApiV1\Modules\SellerUsers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateOrReplaceOperatorRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'seller_id' => ['required', 'integer'],
            'user_id' => ['required', 'integer', Rule::unique('operators')->ignore((int) $this->route('operatorId'))],
            'is_receive_sms' => ['nullable', 'boolean'],
            'is_main'  => ['nullable', 'boolean'],
      ];
    }
}
