<?php

namespace App\Http\ApiV1\Modules\Stores\Queries;

use App\Domain\Stores\Models\StorePickupTime;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class StorePickupTimesQuery extends QueryBuilder
{
    public function __construct(Request $request)
    {
        $query = StorePickupTime::query();

        parent::__construct($query, new Request($request->all()));

        $this->allowedIncludes([]);

        $this->allowedSorts([
            'id',
            'store_id',
            'day',
            'pickup_time_code',
            'pickup_time_start',
            'pickup_time_end',
            'cargo_export_time',
            'delivery_service',
            'created_at',
            'updated_at',
        ]);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('store_id'),
            AllowedFilter::exact('day'),
            AllowedFilter::exact('pickup_time_code'),
            AllowedFilter::exact('pickup_time_start'),
            AllowedFilter::exact('pickup_time_end'),
            AllowedFilter::exact('cargo_export_time'),
            AllowedFilter::exact('delivery_service'),
        ]);

        $this->defaultSort('id');
    }
}
