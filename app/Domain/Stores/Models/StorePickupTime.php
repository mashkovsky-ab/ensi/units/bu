<?php

namespace App\Domain\Stores\Models;

use Carbon\Carbon;
use Database\Factories\Stores\StorePickupTimeFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Класс-модель для сущности "Склады - время отгрузки грузов"
 * Class StorePickupTime
 *
 * @property int $id
 * @property int $store_id - id склада
 * @property int $day - день недели (1-7)
 * @property string $pickup_time_code - код времени отгрузки у службы доставки
 * @property Carbon $pickup_time_start - время начала отгрузки
 * @property Carbon $pickup_time_end - время окончания отгрузки
 * @property Carbon $cargo_export_time - время выгрузки информации о грузе в службу доставки
 * @property int $delivery_service - служба доставки (если указана, то данная информация переопределяет данные дня недели без службы доставки) PS: завязана с логистикой, пока не знаю нужна ли - оставляем
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class StorePickupTime extends Model
{
    use HasFactory;

    /**
     * Заполняемые поля модели
     */
    const FILLABLE = [
        'store_id',
        'day',
        'pickup_time_code',
        'pickup_time_start',
        'pickup_time_end',
        'cargo_export_time',
        'delivery_service',
    ];

    protected $fillable = self::FILLABLE;

    protected $table = 'store_pickup_times';

    /**
     * Create a new factory instance for the model.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    protected static function newFactory()
    {
        return StorePickupTimeFactory::new();
    }
}
