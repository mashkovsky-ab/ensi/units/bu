<?php

namespace App\Domain\Stores\Actions;

use App\Domain\Stores\Models\StoreWorking;

class DeleteStoreWorkingAction
{
    public function execute(int $storeWorkingId): void
    {
        StoreWorking::destroy($storeWorkingId);
    }
}
