<?php

namespace App\Domain\SellerUsers\Actions;

use App\Domain\SellerUsers\Models\Operator;
use Illuminate\Support\Arr;

class ReplaceOperatorAction
{
    public function execute(int $operatorId, array $fields): Operator
    {
        $operator = Operator::findOrFail($operatorId);

        $newAttributes = Arr::only($fields, Operator::FILLABLE);
        foreach (Operator::FILLABLE as $attributeKey) {
            if (!array_key_exists($attributeKey, $newAttributes)) {
                $newAttributes[$attributeKey] = null;
            }
        }
        $operator->update($newAttributes);

        return $operator;
    }
}
