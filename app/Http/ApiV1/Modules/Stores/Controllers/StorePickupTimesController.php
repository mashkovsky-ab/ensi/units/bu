<?php

namespace App\Http\ApiV1\Modules\Stores\Controllers;

use App\Domain\Stores\Actions\CreateStorePickupTimeAction;
use App\Domain\Stores\Actions\DeleteStorePickupTimeAction;
use App\Domain\Stores\Actions\PatchStorePickupTimeAction;
use App\Domain\Stores\Actions\ReplaceStorePickupTimeAction;
use App\Http\ApiV1\Modules\Stores\Queries\StorePickupTimesQuery;
use App\Http\ApiV1\Modules\Stores\Requests\CreateOrReplaceStorePickupTimeRequest;
use App\Http\ApiV1\Modules\Stores\Requests\PatchStorePickupTimeRequest;
use App\Http\ApiV1\Modules\Stores\Resources\StorePickupTimesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class StorePickupTimesController
{
    public function create(CreateOrReplaceStorePickupTimeRequest $request, CreateStorePickupTimeAction $action)
    {
        return new StorePickupTimesResource($action->execute($request->validated()));
    }

    public function get(int $storePickupTimeId, StorePickupTimesQuery $query)
    {
        return new StorePickupTimesResource($query->findOrFail($storePickupTimeId));
    }

    public function replace(int $storePickupTimeId, CreateOrReplaceStorePickupTimeRequest $request, ReplaceStorePickupTimeAction $action)
    {
        return new StorePickupTimesResource($action->execute($storePickupTimeId, $request->validated()));
    }

    public function patch(int $storePickupTimeId, PatchStorePickupTimeRequest $request, PatchStorePickupTimeAction $action)
    {
        return new StorePickupTimesResource($action->execute($storePickupTimeId, $request->validated()));
    }

    public function delete(int $storePickupTimeId, DeleteStorePickupTimeAction $action)
    {
        $action->execute($storePickupTimeId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, StorePickupTimesQuery $query)
    {
        return StorePickupTimesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
