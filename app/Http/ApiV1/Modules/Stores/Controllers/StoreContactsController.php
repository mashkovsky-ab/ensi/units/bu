<?php

namespace App\Http\ApiV1\Modules\Stores\Controllers;

use App\Domain\Stores\Actions\CreateStoreContactAction;
use App\Domain\Stores\Actions\DeleteStoreContactAction;
use App\Domain\Stores\Actions\PatchStoreContactAction;
use App\Domain\Stores\Actions\ReplaceStoreContactAction;
use App\Http\ApiV1\Modules\Stores\Queries\StoreContactsQuery;
use App\Http\ApiV1\Modules\Stores\Requests\CreateOrReplaceStoreContactRequest;
use App\Http\ApiV1\Modules\Stores\Requests\PatchStoreContactRequest;
use App\Http\ApiV1\Modules\Stores\Resources\StoreContactsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;

class StoreContactsController
{
    public function create(CreateOrReplaceStoreContactRequest $request, CreateStoreContactAction $action)
    {
        return new StoreContactsResource($action->execute($request->validated()));
    }

    public function get(int $storeContactId, StoreContactsQuery $query)
    {
        return new StoreContactsResource($query->findOrFail($storeContactId));
    }

    public function replace(int $storeContactId, CreateOrReplaceStoreContactRequest $request, ReplaceStoreContactAction $action)
    {
        return new StoreContactsResource($action->execute($storeContactId, $request->validated()));
    }

    public function patch(int $storeContactId, PatchStoreContactRequest $request, PatchStoreContactAction $action)
    {
        return new StoreContactsResource($action->execute($storeContactId, $request->validated()));
    }

    public function delete(int $storeContactId, DeleteStoreContactAction $action)
    {
        $action->execute($storeContactId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, StoreContactsQuery $query)
    {
        return StoreContactsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
